﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace BugTracker
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class ClientService : WebService
    {
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<Project> GetProjects()
        {
            return new DatabaseService().GetProjects().ToList();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Project GetProject(int projectId)
        {
            return new DatabaseService().GetProject(projectId);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<Bug> GetAllBugs()
        {
            return new DatabaseService().GetBugs().ToList();
        }

        [WebMethod]
        public void SaveProject(Project project)
        {
            new DatabaseService().SaveProject(project);
        }

        [WebMethod]
        public void SaveBug(Bug bug)
        {
            new DatabaseService().SaveBug(bug);
        }

        [WebMethod]
        public void UpdateBugState(int projectId, int bugId, int state)
        {
            new DatabaseService().UpdateBugState(projectId, bugId, state);
        }
    }
}
