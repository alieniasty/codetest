﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace BugTracker.Hubs
{
    public class ProjectHub : Hub
    {
        protected ProjectHub()
        {
        }

        public void NewProject(string message)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage();
        }
    }
}