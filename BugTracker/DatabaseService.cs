﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BugTracker
{
    public class DatabaseService
    {
        private static readonly DatabaseDataContext _dbContext = new DatabaseDataContext(
            ConfigurationManager.ConnectionStrings["BugTrackerConnectionString"].ConnectionString);
        
        public IEnumerable<Project> GetProjects()
        {
            return _dbContext.Projects;
        }

        public Project GetProject(int id)
        {
            return _dbContext.Projects.SingleOrDefault(p => p.Id == id);
        }

        public IEnumerable<Bug> GetBugs()
        {
            return _dbContext.Bugs;
        }

        public Bug GetBug(int id)
        {
            return _dbContext.Bugs.SingleOrDefault(b => b.Id == id);
        }

        public void SaveProject(Project project)
        {
            _dbContext.Projects.InsertOnSubmit(project);
            _dbContext.SubmitChanges();
        }

        public void SaveBug(Bug bug)
        {
            var project = _dbContext.Projects.SingleOrDefault(p => p.Id == bug.ProjectId);
            project?.Bugs.Add(bug);
            _dbContext.SubmitChanges();
        }

        public void UpdateBugState(int projectId, int bugId, int state)
        {
            var bugToUpdate = _dbContext
                .Projects
                .Single(p => p.Id == projectId)
                .Bugs
                .Single(b => b.Id == bugId);

            if (bugToUpdate != null) bugToUpdate.State = state;
            _dbContext.SubmitChanges();
        }
    }
}