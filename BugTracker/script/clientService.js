﻿(function() {
    function ClientService() {

        function callServer(method, args) {
            var deferred = $.Deferred();
            var data = args ? JSON.stringify(args) : "{}";
            $.ajax({
                type: 'POST',
                url: '/ClientService.asmx/' + method,
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error: function (response) {
                    alert(response.responseText);
                }
            }).done(function (res) {
                deferred.resolve(res.d);
            });
            return deferred;
        }

        this.getProjects = function() {
            return callServer("GetProjects");
        };

        this.getProject = function(id) {
            return callServer("GetProject", { id: id });
        };

        this.getBugs = function() {
            return callServer("GetBugs");
        };

        this.saveBug = function (bug) {
            return callServer("SaveBug", { bug: bug });
        };

        this.updateBugState = function (projectId, bugId, state) {
            return callServer("UpdateBugState", { projectId: projectId, bugId: bugId, state: state });
        };

        this.saveProject = function (project) {
            return callServer("SaveProject", { project: project });
        };
    }

    window.clientService = new ClientService();
})();