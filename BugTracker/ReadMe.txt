﻿
Project Description:

In this project the user is able to create projects. For each project it's possible to create a number of bugs. 

The project list in the left hand side, should reflect the state of each project. If a project contains any active bugs ( bugs which are new or in progress),
it will show a red icon, otherwise it will show a green check mark.

Right now entities are only created in-memory. 

Task 1:
Persist newly created projects in the database. - check

Task 2:
Persist newly created bugs / changed bugs in the database. - check

Task 3:
When the state of a bug is changed, the parent view should be updated. Right now this is done by calling parent.parent.update(). 
	a: In a few words explain why this is a bad approach.
	b: offer a new approach so that parent views are updated. You may choose between using global events, or eventhandlers. Explain your choice

Bonus Tasks:

1. Write a javascript snippet that turns the 'B' letters orange in the main headline. - check
2. Convert the bugtracker.css styles to sass (.scss) and use variables for colors / fonts - check


References: 

http://riotjs.com/ - everything about riotjs

https://github.com/voorhoede/riotjs-style-guide - good points on naming conventions

Trouble Running the site?

Change the connection string "AttachDbFilename" to full path and try again

Comments:

1. Entity Framework - brak + wielka szkoda, że nie code first.
2. Dependency injection - brak a szkoda, dodałbym.
3. WebServices - ok ale szkoda, że udostępniają zasoby jak REST a nie logikę biznesową.
4. Zmieniłem FirstOrDefault na SingleOrDefault. Id projektu powinno być unikalne więc oczekiwałbym wyjątku który SingleOrDefault rzuci gdy jakimś cudem Id będzie zduplikowane. 
5. DatabaseService mógłby być lepiej nazwany ponieważ de facto jest on repozytorium (repository patter), nie serwisem.
6. Nie powinno się udostępniać klientowi tak szczegółowych wyjątków jakie zwraca usługa. 
Między innymi dlatego byłbym w tym rozwiązaniu zwolennikiem RESTowego Web API- żeby zwrócić klasyczne kody odpowiedzi HTTP. 
Ewentualnie przechwycić wyjątek ale z tego co widzę nie byłoby to łatwe gdyż w przypadku podania złego endpointa (lub nie podania parametru do właściwego endpointa), jako klient dostajemy "wygadany" stack trace jeszcze przed wejściem w Web Method.
7. RiotJs - bardzo przyjemny framework. Przypomina Angulara 1.6.
